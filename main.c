/*
 * Copyright 2019 Walid M. Boudelal
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http:www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_NUMBERS 1024

static void merge_sort_(int *numbers, size_t n, int *buf);
void merge_sort(int *numbers, size_t n);
int *get_random_numbers(size_t n);
void print_numbers(const int *numbers, size_t n);

/* TODO
 * - Generalize this function. Make it work for any type. You'd have to pass
 *   in the size of one item as an extra paramater, but it's so worth it.
 * - Generalize it by also allowing the user to pass in his own comparison
 *   function, and not just hardcoding it in the function itself.
 * - Remember that I'm proud of you for having done this :D
 */

int main(int argc, char *argv[])
{
	int *numbers;
	size_t n = argc > 1 ? strtoul(argv[1], NULL, 0) : 24;

	n = n > 0 && n <= MAX_NUMBERS ? n : MAX_NUMBERS;

	if (!(numbers = get_random_numbers(n))) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}

	puts("Before sorting:");
	print_numbers(numbers, n);

	merge_sort(numbers, n);

	puts("After sorting:");
	print_numbers(numbers, n);

	free(numbers);

	exit(EXIT_SUCCESS);
}

void merge_sort(int *numbers, size_t n)
{
	int *const buf = malloc(n * sizeof *buf);

	if (!buf) {
		perror("malloc");
		free(numbers);
		exit(EXIT_FAILURE);
	}

	merge_sort_(numbers, n, buf);
	free(buf);
}

static void merge_sort_(int *numbers, size_t n, int *buf)
{
	size_t left_len, right_len, l, r, i;
	int *left, *right;

	if (n == 1)
		return;

	left = numbers;
	left_len = n / 2;

	right = left + left_len;
	right_len = n - left_len;

	merge_sort_(left, left_len, buf);
	merge_sort_(right, right_len, buf);

	l = r = i = 0;

	while (l < left_len && r < right_len && i < n) {
		if (left[l] < right[r])
			buf[i] = left[l++];
		else
			buf[i] = right[r++];
		++i;
	}

	if (l < left_len)
		memcpy(buf + i, left + l, sizeof *buf * (left_len - l));
	else if (r < right_len)
		memcpy(buf + i, right + r, sizeof *buf * (right_len - r));

	memcpy(numbers, buf, n * sizeof *buf);
}

int *get_random_numbers(size_t n)
{
	int *nums = malloc(n * sizeof *nums);
	size_t i;
	const int rand_range_max = 1024;
	const int rand_range_half = rand_range_max / 2;

	if (!nums)
		return NULL;

	srand((unsigned) time(NULL));

	for (i = 0; i < n; ++i)
		nums[i] = (rand() % rand_range_max) - rand_range_half;

	return nums;
}

void print_numbers(const int *numbers, size_t n)
{
	if (n == 1) {
		printf("%d\n", *numbers);
		return;
	}

	printf("%d, ", *numbers);

	print_numbers(numbers + 1, n - 1);
}
